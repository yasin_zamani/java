/**
 * @author yzk
 * @version 2.2
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;


public class Application {

    /**
     * Displays a menu for user selection
     */
   /* public void customerOrAdmin() {

        System.out.println("Are you a Customer or Admin?");
        Scanner input = new Scanner(System.in);
        String admin_customer = input.next();
        if (admin_customer.compareToIgnoreCase("Admin") == 0) {
            menu();

        } else if (admin_customer.compareToIgnoreCase("Customer") == 0) {
            Boolean isCont = true;
            while (isCont != false) {
                System.out.println("Please Select any option: \nPress 1 to Register Vehicle. \nPress 2 to Collect Vehicle. \nPress n to exit(Save)");
                String menuChoice = input.nextLine();
                if (menuChoice.compareToIgnoreCase("1") == 0) {
                    registerVehicle();
                } else if (menuChoice.compareToIgnoreCase("2") == 0) {
                    collectVehicle();
                }
                if (menuChoice.compareToIgnoreCase("n") == 0) {
                    isCont = false;
                    Zones.save();
                }
            }
        }else{
            System.err.println("Select Valid Selection");
        }
    }*/
    public void menu() {
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("~~~~Welcome To Multi-Story, Car-Parking Program~~~~~");
        System.out.println("~~~~~~~~~~~~~~~~~Created By Yasin~~~~~~~~~~~~~~~~~~~");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        Scanner input = new Scanner(System.in);
        Boolean isCont = true;
        while (isCont != false) {

            System.out.println("Please Select any option: \nPress 1 to Register Vehicle. \nPress 2 to Collect Vehicle."
                    + " \n Admin : Press 3 to add new attendant. \n Admin : Press 4 to remove an attendant. \n Admin : Press 5 to activate attendant."
                    + "\n Admin : Press 6 to activate all attendants. \n Admin : Press 7 to deactivate attendant. \n Admin : Press 8 to deactivate all attendants."
                    + "\n Admin : Press 9 to View all Vehicles stored. \n Admin : Press 0 to store state. \n Admin : Press n to exit from the system. ");
            String menuChoice = input.nextLine();
            if (menuChoice.compareToIgnoreCase("1") == 0) {
                registerVehicle();
            } else if (menuChoice.compareToIgnoreCase("2") == 0) {
                collectVehicle();
            } else if (menuChoice.compareToIgnoreCase("3") == 0) {
                addAttendant();
            } else if (menuChoice.compareToIgnoreCase("4") == 0) {
                removeAttendant();
            } else if (menuChoice.compareToIgnoreCase("5") == 0) {
                updateAttendant(1);
            } else if (menuChoice.compareToIgnoreCase("6") == 0) {
                updateAllAttendants(1);
            } else if (menuChoice.compareToIgnoreCase("7") == 0) {
                updateAttendant(0);
            } else if (menuChoice.compareToIgnoreCase("8") == 0) {
                updateAllAttendants(0);
            } else if (menuChoice.compareToIgnoreCase("9") == 0) {
                readAllData();
            } else if (menuChoice.compareToIgnoreCase("n") == 0) {
                Zones.save();
                break;
            } else if(menuChoice.compareToIgnoreCase("0") == 0){
                Zones.save();
            }
            else {
                System.out.println("Please select a valid option.");
            }
            //Asking user if continue with the program or exit
            System.out.println("");
            System.out.println("Do you want to continue? Press n to exit or any other button to continue!");
            String contChoice = input.nextLine();
            if (contChoice.compareToIgnoreCase("n") == 0) {
                Zones.save();
                break;
            }
        }
    }

    /**
     * Ask user if he/she want to register by himself/herself or want to hire parking attendant
     */

    public void registerVehicle() {
        Scanner input = new Scanner(System.in);
        System.out.println("");
        System.out.println("Do you want to Register your vehicle by yourself or want to hire some parking attendant? \nPress 1 to Register Vehicle By Yourself. \nPress 2 to hire any Parking Attendant.");
        String registerByChoice = input.nextLine();
        if (registerByChoice.compareToIgnoreCase("1") == 0) {
            registerByCustomer();
        } else if (registerByChoice.compareToIgnoreCase("2") == 0) {
            registerByAttendant();
        } else {
            System.out.println("Please Select Valid Option.");
        }
    }

    /**
     * ask user if he/she want to collect the vehicle by himself/herself or want to hire parking attendant
     */
    public void collectVehicle() {
        Scanner input = new Scanner(System.in);
        System.out.println("");
        System.out.println("Do you want to Collect your vehicle by yourself or want to hire some parking attendant? \nPress 1 to Collect Vehicle By Yourself. \nPress 2 to hire any Parking Attendant.");
        String collectByChoice = input.nextLine();
        if (collectByChoice.compareToIgnoreCase("1") == 0) {
            collectByCustomer();
        } else if (collectByChoice.compareToIgnoreCase("2") == 0) {
            collectByAttendant();
        } else {
            System.out.println("Please Select Valid Option.");
        }
    }

    /**
     * Collect data when user want to park/store the vehicle
     * @param selection choice of user 0 means attendant 1 means your self
     */
    public void getInfoForRegister(int selection) {
        Attendant attObjReg = new Attendant();
        Boolean attHired = false;
        if (selection == 0) {//0 means register through attendant
            String attToHire = attObjReg.getAttendant();
            if (attToHire == "") {
                System.out.println("All attendants are busy. Please Register it by yourself.");
                selection = 1;
                attHired = false;
            } else {
                System.out.println("Attendant with ID # " + attToHire + " Is Hired to Register the Vehicle...!");
                attHired = true;
            }
        }
        Random random = new Random();
        Scanner input = new Scanner(System.in);
        MainVehicle vObj = new MainVehicle();
        MainVehicle vehicleObj = null;
        Zones zoneObject = null;
        System.out.println("Please enter your License number.");
        String lNum = input.nextLine();
        Boolean isExist = vObj.vehicleExist(lNum);
        if (isExist == false) {
            System.out.println("Please select the type of vehicle: ");
            System.out.println("1. Standard sized vehicle (up to 2 metres in height and less than 5 metres in length: car or Small Van)");
            System.out.println("2. Higher vehicle (over 2 metres in height but less then 3 metres in height and less than 5 metres in length: Tall Short Wheel-Based Van).");
            System.out.println("3. Longer vehicle (less then 3 metres in height and between 5.1 and 6 metres in length: Long Wheel-Based Van).");
            if (selection == 1 || attHired == false) {
                System.out.println("4. Coach.");
                System.out.println("5. Motorbike. ");
            }
            if (selection == 0) {
                System.out.println("Info from Customer to Attendant :");
            }
            System.out.println("");
            String vehicleChoice = input.nextLine();
            System.out.println("Vehicle Information and Location:");
            //String timeArrival = getTime();
            switch (vehicleChoice) {
                case "1":
                    if (random.nextInt(2) == 1) {
                        zoneObject = new Zones("Inside", "Standard Sized Zone", "Multi-story building", "Standard Sized Vehicle", 1, 1);
                    } else {
                        zoneObject = new Zones("Outside", "Standard Sized Zone", "Not in Multistory Building", "Standard Sized Vehicle", 1, 4);
                    }
                    vehicleObj = new SmallVan("Standard Sized Vehicle", "Car/Small Van", getTime().toString(), lNum, zoneObject);
                    System.out.println(vehicleObj.toString());
                    break;
                case "2":
                    zoneObject = new Zones("Outside", "Standard Sized Zone", "Not in Multistory Building", "Higher Sized Vehicle", 1, 1);
                    vehicleObj = new ShortVan("Higher Sized Vehicle", "Tall Short Wheel-Based Van", getTime().toString(), lNum, zoneObject);
                    System.out.println(vehicleObj.toString());
                    break;
                case "3":
                    zoneObject = new Zones("Outside", "Longer Sized Zone", "Not in Multistory Building", "Longer Sized Vehicle", 1.5, 2);
                    vehicleObj = new LongVan("Longer Sized Vehicle", "Long Wheel-Based Van", getTime().toString(), lNum, zoneObject);
                    System.out.println(vehicleObj.toString());
                    break;
                case "4":
                    if (selection == 1) {
                        zoneObject = new Zones("Outside", "Coach Zone", "", "Just for Coaches", 2, 3);
                        vehicleObj = new Coach("Coach Sized Vehicle", "Coach", getTime().toString(), lNum, zoneObject);
                        System.out.println(vehicleObj.toString());
                        break;
                    } else {
                        System.out.println("Please enter valid number.");
                        break;
                    }
                case "5":
                    if (selection == 1) {
                        zoneObject = new Zones("Inside", "Motorbikes Zone", "Multi-story building", "Motorbikes Only", 0.5, 5);
                        vehicleObj = new Motorbike("Motorbike Sized Vehicle", "Motorbike", getTime().toString(), lNum, zoneObject);
                        System.out.println(vehicleObj.toString());
                        break;
                    } else {
                        System.out.println("Please enter valid number.");
                        break;
                    }
            }
        } else {
            System.out.println("Vehicle with license number " + lNum + " is already stored. Please enter another license number.");
        }
    }

    /**
     * /generate current system time
     * @return the time of our system.
     */
    public String getTime() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date();
        String dateStr = dateFormat.format(date).toString();
        return dateStr;
    }

    /**
     * Collect data when user want to collect the vehicle
     * @param index choice of user 0 to hire attendant 1 to self register.
     */
    public void getInfoToCollect(int index) {
        Attendant attObj = new Attendant();
        Boolean attHired = false;
        if (index == 0) {
            String attToHire = attObj.getAttendant();
            if (attToHire == "") {
                System.out.println("All attendants are busy. Please Collect it by yourself.");
                index = 1;
                attHired = false;
            } else {
                System.out.println("Attendant with ID # " + attToHire + " Is Hired to Collect the Vehicle...!");
                attHired = true;
            }
        }
        Scanner input = new Scanner(System.in);
        MainVehicle vehicleObj = new MainVehicle();
        Zones zoneObj = null;
        System.out.println("Please enter your License number.");
        String licenseNum = input.nextLine();
        Boolean isExist = vehicleObj.vehicleExist(licenseNum);
        if (isExist == true) {
            if (attHired == true) {
                System.out.println("Attendants Here");
            }
            System.out.println("Please enter Receipt number.");
            String rcpNo = input.nextLine();
            Boolean isRcpExist = vehicleObj.getReceipt(licenseNum, rcpNo);
            if (isExist == false) {
                System.err.println("Vehicle does not exist!");
            }//if license exist
            if (isRcpExist == true) {
                System.out.println("Please pay your fee, if you pay over the limit it will give you back the change");
                double getBill = vehicleObj.getHourlyRate(licenseNum);
                System.out.println("Customer Here ...!");
                System.out.println("Please Pay your Parking Fee");

                String amount = input.nextLine();
                double x = getBill;
                double y = Double.parseDouble(amount);
                double z = y - x;
                DecimalFormat df = new DecimalFormat("#.##");
                double change = Double.valueOf(df.format(z));


                if (z >= 0) {
                    if (amount.compareToIgnoreCase(x + "") == 0) {
                        System.out.println("Paid");
                    } else {
                        System.out.println("Please take your change back : " + change);
                    }
                } else {
                    System.out.println("Please Enter Full Money and try again!"); // didn't wanted to do loop here since the price changes.
                    return;
                }
                System.out.println("Vehicle is Leaving ... ");
                Boolean isUpdated = vehicleObj.removeVehicle(licenseNum);
                for (int i = 0; i < 3; i++) {
                    try {
                        //Pause for 1 seconds
                        Thread.sleep(1000);
                        //Print a message
                        System.out.println(" . . . ");
                    } catch (InterruptedException ex) {
                    }
                }
                if (isUpdated == true) {
                    System.out.println("Vehicle has leaved Successfully");
                } else {
                    System.out.println("Some error in updating file.");
                }
            }
        }
    }

    /**
     * if user want to register by himself/herself then direct him to method to take necessary info to store/park the vehicle
     */
    public void registerByCustomer() {
        getInfoForRegister(1);
    }

    /**
     * if user want to register by hiring attendant then direct him to method to take necessary info to store/park the vehicle
     */
    public void registerByAttendant() {
        getInfoForRegister(0);
    }

    /**
     * if user want to collect by himself/herself then direct him to method to take necessary info to store/park the vehicle
     */
    public void collectByCustomer() {
        getInfoToCollect(1);
    }

    /**
     * if user want to collect by hiring attendant then direct him to method to take necessary info to store/park the vehicle
     */
    public void collectByAttendant() {
        getInfoToCollect(0);
    }

    /**
     * /read all the data of vehicles currently parked/stored
     */
    public void readAllData() {
        File file = new File("vehicles.txt");
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String rec = "";

            while ((rec = reader.readLine()) != null) {
                String[] splitRec = rec.split("~~");
                System.out.println("License Number : " + splitRec[0] + " , Vehicle Name : " + splitRec[2] + " , Vehicle Size: " + splitRec[1] + " , Parking Receipt: " + splitRec[4] + " , Space ID: " + splitRec[3] + " , Zone Building : " + splitRec[5] + " , Zone Location: " + splitRec[6] + " , Parking Charges : " + splitRec[7]);
            }

        } catch (Exception ex) {
            System.out.println(ex);
        }

    }

    /**
     * method used to add attendant to file. It calls addAttendant(id) method in the Attendant class which add the record to file
     */
    public void addAttendant() {
        Attendant att = new Attendant();
        Scanner input = new Scanner(System.in);
        System.out.println("Please Enter ID of attendant :");
        String id = input.nextLine();
        Boolean isExist = att.addAttendant(id); //returns true if attendant exist
        if (isExist == true) {
            System.out.println("Please enter another id number for attendant. " + id + " Already exists.");
        } else {
            System.out.println("Attendant added Successfully. " + "ID : " + id);
        }
    }

    /**
     * method used to remove attendant from file. It calls deleteAtt(id) method in the Attendant class which deletes that record from file
     */
    public void removeAttendant() {
        Attendant att = new Attendant();
        Scanner input = new Scanner(System.in);
        System.out.println("Please Enter ID of attendant :");
        String id = input.nextLine();
        Boolean isExist = att.isAttExist(id); //returns true if attendant exist
        if (isExist == true) {
            Boolean isDel = att.deleteAtt(id);
            if (isDel == true) {
                System.out.println("Successfully Deleted");
            } else {
                System.out.println("Not deleted Successfully. ");
            }
        } else {
            System.out.println("Attendant with ID # " + id + " doesn't exist exist.");
        }
    }

    /**
     *      method used to update status (Available or Busy) of specific attendant. It calls updateAtt(id,index) method from Attendant class
     *      index 0 means deactivate attendant
     *      index 1 means activate attendant
     * @param index choice of admin to activate(1) or deactivate(0) attendant
     */
    public void updateAttendant(int index) {
        Attendant att = new Attendant();
        Scanner input = new Scanner(System.in);
        System.out.println("Please Enter ID of attendant :");
        String id = input.nextLine();
        Boolean isExist = att.isAttExist(id); //returns true if attendant exist
        if (isExist == false) {
            System.out.println("Attendant with ID # " + id + " is not exist.");
        } else {
            Boolean isUpdated = att.updateAtt(id, index); //returns true if updated
            if (isUpdated == true) {
                if (index == 0) {
                    System.out.println("Attendant with ID # " + id + " is Deactivated now.");
                } else if (index == 1) {
                    System.out.println("Attendant with ID # " + id + " is Available now.");
                }
            } else {
                System.out.println("Doesn't Exist");
            }
        }

    }

    /**
     *     method used to update status (Available or Busy) of ALL attendant. It calls updateAllAtt() method from Attendant class
     *
     * @param index Index 0 means deactivate all attendants
     *              index 1 means activate all attendants
     */
    public void updateAllAttendants(int index) {
        Attendant att = new Attendant();
        boolean isUpdated = att.updateAllAtt(index); //returns true if updated
        if (isUpdated == true) {
            if (index == 0) {
                System.out.println("All Attendants are Deactivated now.");
            } else if (index == 1) {
                System.out.println("All Attendants are Available now.");
            }
        } else {
            System.out.println("Some error with file while updating.");
        }
    }

    /**
     *  Our Application runner.
     * @param args array of string
     */
    public static void main(String args[]) {
        Application app = new Application();
        Zones.load();
        app.menu();
    }
}