public class LongVan extends MainVehicle {
    public LongVan(String vehSize,String vehName, String timeArr, String licNum, Zones zones){
        // calling super class
        super(vehSize,vehName, timeArr,licNum,zones);

    }
    /**
     * toString method that is overridden by the super class
     * @return this returns toString method from the super class
     */
    // generated Override
    @Override
    public String toString() {
        return super.toString();
    }
}
