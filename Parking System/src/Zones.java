/**
 * @author yzk
 * @version 1.0
 */
import java.io.*;
import java.util.Random;

public class Zones {
    String location = "";
    String zoneLocation = "";
    String zoneBuild = "";
    String vehicleType = "";
    String pReceipt = "";
    String zSize = "";
    double hourlyCharge = 0.0;
    String spaceId = null;


        /*
        The idea here is very simple, we have rows and columns every time it zone is equal to 5,
        it will increment row by 1 and make the col back to 0.

        Column(1) CAR - CAR - CAR - BIKE - CAR (column++)
        Column(2) CAR - CAR - CAR - CAR - CAR (column++)
        Column(3) ....

         */
    static int rowZ1 = 1;
    static int colZ1 = 0;
    static int rowZ2 = 1;
    static int colZ2 = 0;
    static int rowZ3 = 1;
    static int colZ3 = 0;
    static int rowZ4 = 1;
    static int colZ4 = 0;
    static int rowZ5 = 1;
    static int colZ5 = 0;

    public Zones() {
    }

    public Zones(String zoneLocation, String zSize, String zoneBuild, String vehicleType, double hourlyCharge, int zoneId) {
        this.zoneLocation = zoneLocation;
        this.zoneBuild = zoneBuild;
        this.zSize = zSize;
        this.vehicleType = vehicleType;
        this.hourlyCharge = hourlyCharge;
        String integerForReceipt = String.format("%03d", generateInt(255));
        this.spaceId = findZone(zoneId);
        this.pReceipt = "rcp-num : " + integerForReceipt;


    }


    private int generateInt(int number) {
        Random random = new Random();
        return random.nextInt(number);
    }

    /**
     * here we will find our space in zone
     * @param zoneId is our zone to be at in MCP
     * @return the zone
     */
    private String findZone(int zoneId) {
        // basic if statements(if column in zone 1 is equal to 5 increment row of zone 1 and set col to 0)
        if (zoneId == 1) { // space in zone 1
            if (colZ1 == 5) {
                rowZ1++;
                colZ1 = 0;
            }
            colZ1++;
            location = "row :" + rowZ1 + "col :" + colZ1;
        } else if (zoneId == 2) { // space in zone 2
            if (colZ2 == 5) {
                rowZ2++;
                colZ2 = 0;
            }
            colZ2++;
            location = "row :" + rowZ2 + "col :" + colZ2;
        } else if (zoneId == 3) { // space in zone 3
            if (colZ3 == 5) {
                rowZ3++;
                colZ3 = 0;
            }
            colZ3++;
            location = "row :" + rowZ3 + "col :" + colZ3;
        } else if (zoneId == 4) { //space in zone 4
            if (colZ4 == 5) {
                rowZ4++;
                colZ4 = 0;
            }
            colZ4++;
            location = "row :" + rowZ4 + "col :" + colZ4;
        } else if (zoneId == 5) { // space in zone 5
            if (colZ5 == 5) {
                rowZ5++;
                colZ5 = 0;
            }
            colZ5++;
            location = "row :" + rowZ5 + "col :" + colZ5;
        }
        return location;
    }

    /**
     *
     * @return our zone according to our choice
     */
    @Override
    public String toString() {
        return "\n Your Parking Zone is : \n " + "Z-Location = " + zoneLocation + " ,\n" + "Vehicle Zone " + zSize + " ,\n Z-Building = " + zoneBuild + ",\n Vehicle Type = " + vehicleType + "\n Hourly Charge = " + hourlyCharge + "per hour , \n Space ID = " + spaceId + "\n and Receipt number = " + getpReceipt() + "";
    }


    /**
     * this method will save the zone data used in the session with IOException catch / try.
     */
    public static void save() {
        File save = new File("zone.txt");
        BufferedWriter savez = null;
        try {
            savez = new BufferedWriter(new FileWriter(save, false));
            savez.write("Z1," + rowZ1 + "," + colZ1 + "AND" + "Z2," + rowZ2 + "," + colZ2 + "AND" +
                    "Z3," + rowZ3 + "," + colZ3 + "AND" +
                    "Z4," + rowZ4 + "," + colZ4 + "AND" +
                    "Z5," + rowZ5 + "," + colZ5);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                savez.close();
            } catch (IOException e) {

            }
        }
    }

    /**
     * this method will load the data from the last session it was used for
     */
    public static void load() {
        File loadFile = new File("zone.txt");
        BufferedReader loader = null;
        try {
            loader = new BufferedReader(new FileReader(loadFile));
            String[] zArray;
            String replace = "";
            String[] mArray;
            while ((replace = loader.readLine()) != null) {
                zArray = replace.split("AND");
                for (int y = 0; y < zArray.length; y++) {
                    mArray = zArray[y].split(",");
                    String o = mArray[1];
                    String p = mArray[2];
                    int x = Integer.parseInt(o);
                    int z = Integer.parseInt(p);
                    // now assign the values back to the program, easy if statements
                    if (y == 0) {
                        setRowZ1(x);
                        setColZ1(z);
                    }
                    if (y == 1) {
                        setRowZ2(Integer.parseInt(mArray[1]));
                        setColZ2(Integer.parseInt(mArray[2]));
                    }
                    if (y == 2) {
                        setRowZ3(Integer.parseInt(mArray[1]));
                        setColZ3(Integer.parseInt(mArray[2]));
                    }
                    if (y == 3) {
                        setRowZ4(Integer.parseInt(mArray[1]));
                        setColZ4(Integer.parseInt(mArray[2]));
                    }
                    if (y == 4) {
                        setRowZ5(Integer.parseInt(mArray[1]));
                        setColZ5(Integer.parseInt(mArray[2]));
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @return parking receipt
     */
    public String getpReceipt() {
        return this.pReceipt;
    }

    /**
     *
     * @return spaceid
     */
    public String getSpaceId() {
        return this.spaceId;
    }

    /**
     *
     * @return hourly charge by each zone
     */
    public double getHourlyCharge() {
        return this.hourlyCharge;
    }

    /**
     *
     * @param pRecipt set parking receipt
     */
    public void setpRecipt(String pRecipt) {
        this.pReceipt = pRecipt;
    }

    /**
     *
     * @return size of the zone
     */
    public String getzSize() {
        return zSize;
    }

    /**
     *
     * @param zSize sets size of the zone
     */
    public void setzSize(String zSize) {
        this.zSize = zSize;
    }

    /**
     *
     * @return Zone1 row
     */
    public static int getRowZ1() {
        return rowZ1;
    }

    /**
     *
     * @param rowZ1 sets the row for the zone1
     */
    public static void setRowZ1(int rowZ1) {
        Zones.rowZ1 = rowZ1;
    }
    /**
     *
     * @return column of zone 1
     */
    public static int getColZ1() {
        return colZ1;
    }

    /**
     *
     * @param colZ1 sets column for zone 1
     */
    public static void setColZ1(int colZ1) {
        Zones.colZ1 = colZ1;
    }

    /**
     *
     * @return SAME AS ZONE !
     */
    public static int getRowZ2() {
        return rowZ2;
    }

    public static void setRowZ2(int rowZ2) {
        Zones.rowZ2 = rowZ2;
    }

    public static int getColZ2() {
        return colZ2;
    }

    public static void setColZ2(int colZ2) {
        Zones.colZ2 = colZ2;
    }

    public static int getRowZ3() {
        return rowZ3;
    }

    public static void setRowZ3(int rowZ3) {
        Zones.rowZ3 = rowZ3;
    }

    public static int getColZ3() {
        return colZ3;
    }

    public static void setColZ3(int colZ3) {
        Zones.colZ3 = colZ3;
    }

    public static int getRowZ4() {
        return rowZ4;
    }

    public static void setRowZ4(int rowZ4) {
        Zones.rowZ4 = rowZ4;
    }

    public static int getColZ4() {
        return colZ4;
    }

    public static void setColZ4(int colZ4) {
        Zones.colZ4 = colZ4;
    }

    public static int getRowZ5() {
        return rowZ5;
    }

    public static void setRowZ5(int rowZ5) {
        Zones.rowZ5 = rowZ5;
    }

    public static int getColZ5() {
        return colZ5;
    }

    public static void setColZ5(int colZ5) {
        Zones.colZ5 = colZ5;
    }
}


