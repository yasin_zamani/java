/**
 * @author yzk
 * @version 1.1
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;


public class Attendant {
    String id = null;
    ArrayList attList = new ArrayList();

    /**
     * used to add new attendant to a file
     * @param info attendant name
     * @return boolean value of attendant exist or not
     */
    public Boolean addAttendant(String info){
        Boolean isExist = isAttExist(info);
        if(isExist == true){
            return true;
        }
        String infoToSore = info + "~~" +"Available";
        storeAttendant(infoToSore);
        return false;
    }

    /**
     * loads all the available attendants to ArrayList
     */
    public void loadAttendants(){
        try {
            File file = new File("Attendants.txt");
            BufferedReader reader = null;
            reader = new BufferedReader(new FileReader(file));
            String rec = "";
            String[] myArray;
            while((rec = reader.readLine()) != null){
                myArray = rec.split("~~");
                if(myArray[1].compareToIgnoreCase("Available")==0){
                    attList.add(myArray[0]);
                }
            }
            reader.close();
        } catch (Exception ex) {
        }

    }

    /**
     * store all attendant in a file named as 'Attendants.txt'
     * @param info attendant name
     */
    public void storeAttendant(String info){
        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter(("Attendants.txt"), true));
            writer.write(info);
            writer.write(System.getProperty("line.separator"));
            writer.close();
        }catch(IOException e){
            System.out.println(e);
        }
    }

    /**
     * to get any of the attendant available for registration/collection of vehicle
     * @return empty string
     */
    public String getAttendant(){
        loadAttendants();//just fill the attList with all available attendants
        Random random = new Random();
        if(attList.size() >= 1){
            int x = random.nextInt(attList.size());
            String att = attList.get(x).toString();
            return att;
        }
        return "";
    }

    /**
     * to check if specific attendant exist or not
     * @param info attendant name
     * @return boolean value if attendant exist or not
     */
    public boolean isAttExist(String info){
        File file = new File("Attendants.txt");
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String rec = "";
            while((rec=reader.readLine())!=null){
                String[] splitRec = rec.split("~~");
                if(splitRec[0].compareToIgnoreCase(info) == 0){
                    return true;
                }
            }
        } catch (Exception ex) {
        }
        return false;
    }

    /**
     * used to delete specific attendant
     * @param id attendant name
     * @return boolean value to delete
     */
    public Boolean deleteAtt(String id) {
        File file = new File("Attendants.txt");
        BufferedReader reader = null;
        Boolean isDel = false;
        ArrayList<String> allRec = new ArrayList<String>();
        try {
            reader = new BufferedReader(new FileReader(file));
            String rec = "";
            while((rec=reader.readLine())!=null){
                String[] splitRec = rec.split("~~");
                if(splitRec[0].compareToIgnoreCase(id) == 0){
                    //leave it because we want to delete that record
                }else{//add all other records to the list
                    allRec.add(rec);
                }
            }
            isDel = writeToFile(allRec); //write list of all record to file
        } catch (Exception ex) {
        }
        return isDel;
    }

    /**
     * index 0 means deactivate or busy, 1 means activate or available
     * @param info attendant name
     * @param index is the value between 1 or 0
     * @return the boolean value of Busy / Available.
     */
    //it updates the status of specific attendant accordingly
    public boolean updateAtt(String info, int index){
        File file = new File("Attendants.txt");
        BufferedReader reader = null;
        Boolean isUpdated = false;
        ArrayList<String> allRec = new ArrayList<String>();
        try {
            reader = new BufferedReader(new FileReader(file));
            String rec = "";
            while((rec=reader.readLine())!=null){
                String[] splitRec = rec.split("~~");
                if(splitRec[0].compareToIgnoreCase(info) == 0){//attendant found, now update its status
                    if(index == 0){
                        allRec.add(info +"~~"+ "Busy");
                    }else if(index == 1){
                        allRec.add(info +"~~"+ "Available");
                    }
                }else{
                    allRec.add(rec);
                }
            }
            isUpdated = writeToFile(allRec); //write list of all record to file
        } catch (Exception ex) {
        }
        return isUpdated;
    }

    /**
     *     index 0 means deactivate or busy, 1 means activate or available
     *     it updates the status of all attendant accordingly
     * @param index Status or attendant(0 busy, 1 activate)
     * @return boolean to see if busy or not
     */
    public boolean updateAllAtt(int index){
        File file = new File("Attendants.txt");
        BufferedReader reader = null;
        Boolean isUpdated = false;
        ArrayList<String> allRec = new ArrayList<String>();
        try {
            reader = new BufferedReader(new FileReader(file));
            String rec = "";
            while((rec=reader.readLine())!=null){
                String[] splitRec = rec.split("~~");
                if(index == 0){ //deactivate all
                    allRec.add(splitRec[0] +"~~"+ "Busy");
                }else if(index == 1){//activate all
                    allRec.add(splitRec[0] +"~~"+ "Available");
                }
            }
            isUpdated = writeToFile(allRec); //write list of all record to file
        } catch (Exception ex) {
        }
        return isUpdated;
    }

    /**
     * store all the (remaining) data after deleting any record(of attendant)
     * @param list ArrayList of our attendants
     * @return //  returns the list
     */
    public Boolean writeToFile(ArrayList<String> list){
        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter(("Attendants.txt"), false));
            for(String str:list)
            {
                writer.write(str);
                writer.newLine();
            }
            writer.close();
            return true;

        }catch(IOException e)
        {
            System.out.println(e);
        }
        return false;
    }
}
