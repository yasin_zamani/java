/**
 * @author yzk
 * @version 1.5
 */

import java.io.*;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class MainVehicle {
    String vehName = "";
    String vehSize = "";
    String licNum = "";
    Zones zones = null;

    public MainVehicle() {

    }

    /**
     * Equal method(Never used)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MainVehicle that = (MainVehicle) o;
        return vehName.equals(that.vehName) &&
                vehSize.equals(that.vehSize) &&
                licNum.equals(that.licNum) &&
                zones.equals(that.zones);
    }


    /**
     * Our constructor for creating vehicle
     * @param vehSize vehicle size as a string
     * @param vehName vehicle name as a string
     * @param arrival vehicle arrival
     * @param licNum license number for the vehicle
     * @param zones assign to the zone.
     */
    public MainVehicle(String vehSize, String vehName, String arrival, String licNum, Zones zones) {
        this.vehName = vehName;
        this.vehSize = vehSize;
        this.licNum = licNum;
        this.zones = zones;
        var vehicleInfo = licNum + "~~" + vehSize + "~~" + vehName + "~~" + zones.getSpaceId() + "~~" + zones.getpReceipt() + "~~" + zones.zoneLocation + "~~" + zones.zoneBuild + "~~" + zones.getHourlyCharge() + "~~" + arrival;


        // finally store the data.
        storeData(vehicleInfo);
    }


    @Override
    public String toString() {
        return "Vehicle : " + "Vehicle Size = " + vehSize + ", vehicle Name = " + vehName + ", licenseNum = " + licNum + " " + zones;
    }

    /**
     * return true or false if the car/vehicle exist in the file vehicle.txt
     * @param lNum takes license number as parameter
     * @return returns boolean true or false.
     */
    public boolean vehicleExist(String lNum) {
        File vehicle = new File("vehicles.txt");
        BufferedReader veh = null;
        try {
            veh = new BufferedReader(new FileReader(vehicle));
            String replace = "";
            // same old method used from Zones class instead of AND now ~
            while ((replace = veh.readLine()) != null) {
                String[] splitReplace = replace.split("~~");
                if (splitReplace[0].compareToIgnoreCase(lNum) == 0) { // IF stored
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.err.println("Vehicle Not found in database please proceed where you are now!");
        return false; // if not false
    }

    /**
     * Storing car data when car is parked.
     * @param toStore file to store
     */
    public void storeData(String toStore) {
        try {
            BufferedWriter store = new BufferedWriter(new FileWriter(("vehicles.txt"), true));
            store.write(toStore);
            // Windows platform, System.getProperty("line.separator") is "\r\n",
            store.write(System.getProperty("line.separator"));
            // make sure to close
            store.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println(e);
        }
    }

    /**
     * check if car / vehicle exist in the file to collect
     * @param lNum license number
     * @param receipt receipt number
     * @return boolean to check if the license number and receipt number exist in vehicles.txt
     */
    public boolean getReceipt(String lNum, String receipt) {
        // these are same methods that are used from storeData
        File rcp = new File("vehicles.txt");
        BufferedReader receipts = null;
        try {
            receipts = new BufferedReader(new FileReader(rcp));
            String replace = "";
            while ((replace = receipts.readLine()) != null) {
                String[] splitReplace = replace.split("~~");
                if (splitReplace[0].compareToIgnoreCase(lNum) == 0) {
                    if (splitReplace[4].compareToIgnoreCase("rcp-num : "+receipt) == 0) {
                        System.out.println(splitReplace[4] + "found");
                        return true;
                    } else{
                        System.err.println("Type a valid rcp number");
                        return false;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     *     Parking hourly charge rate, this is not the full implementation!
     *     This covers only 24 hours format.
     * @param lNum license number
     * @return hourly charge rate
     */
    public double getHourlyRate(String lNum) {
        File hourlyRate = new File("vehicles.txt");
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(hourlyRate));
            String replace = "";
            while ((replace = reader.readLine()) != null) {
                String[] splitReplace = replace.split("~~");
                if (splitReplace[0].compareToIgnoreCase(lNum) == 0) { // here we try to find the license
                    System.out.println("");
                    System.out.println(splitReplace[7] + " is the charge");
                    System.out.println("");
                    String arrivalTime[] = splitReplace[8].split(":");
                    int arrivalHour = Integer.parseInt(arrivalTime[0]);
                    int arrivalMinute = Integer.parseInt(arrivalTime[1]);

                    // here we create a clock based on our machine.
                    Date date = new Date();
                    DateFormat dateFormat = new SimpleDateFormat("HH:mm");
                    // Collection Time
                    String collectTime[] = dateFormat.format(date).toString().split(":");
                    int collectHour = Integer.parseInt(collectTime[0]);
                    int collectMin = Integer.parseInt(collectTime[1]);

                    // create two time variables arrival and collection
                    String timeForArrival = arrivalHour + ":" + arrivalMinute;
                    String timeForCollection = collectHour + ":" + collectMin;
                    SimpleDateFormat format;
                    format = new SimpleDateFormat("HH:mm");
                    // calculation for the hourly rate
                    Date date1 = format.parse(timeForArrival);
                    Date date2 = format.parse(timeForCollection);
                    long difference = date2.getTime() - date1.getTime();
                    int diff = (int) (difference / 60000);
                    int minuteSpent = diff;
                    int hour = 0;
                    int day = 0;
                    while (diff > 60) {
                        hour++;
                        diff = diff - 60;
                    }
                   /*
                   Basically no time to finish this, "it should have not been 24 hours parking"
                    while (hour > 23) {
                        day++;
                    }*/
                    System.out.println("Your vehicle has spent : " + hour + " hours and " + diff + " mins at the parking zone");
                    double hourlyCost = Double.parseDouble(splitReplace[7]);
                    double perMinCharge = hourlyCost / 60;
                    double totalCost = perMinCharge * minuteSpent;
                    DecimalFormat df = new DecimalFormat("#.##");
                    double totalCostPerZone = Double.valueOf(df.format(totalCost));
                    //double totalCost = hourlyCost * hour;
                    System.out.println("Your total cost is: " + totalCostPerZone);
                    System.out.println("Your Vehicle is at location :");
                    System.out.println(replace.replaceAll("~~", ","));
                    return totalCostPerZone;
                }
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        int i = 0;
        return i;
    }
    //to get parking rate of vehicle stored in the file


    /**
     * When the vehicle leaves the parking or gets collected by customer / attendant we have to update vehicle.txt
     * @param lNum license number
     * @return boolean value if the car is removed or stored.
     */
    public boolean removeVehicle(String lNum) {
        File delete = new File("vehicles.txt");
        BufferedReader deleter = null;
        ArrayList<String> replace = new ArrayList<String>();
        Boolean y = false;
        try {
            deleter = new BufferedReader(new FileReader(delete));
            String replacey = "";
            while ((replacey = deleter.readLine()) != null) {
                String[] splitReplace = replacey.split("~~");
                if (splitReplace[0].compareToIgnoreCase(lNum) == 0) { // will delete the record

                } else {
                    replace.add(replacey);
                }
            }
            // calling method to write to file
            y = writeToFile(replace);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return y;
    }

    /**
     * iterate through ArrayList to write to our vehicles.txt
     * @param write ArrayList
     * @return boolean value.
     */
    public boolean writeToFile(ArrayList<String> write) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(("vehicles.txt"), false));
            for (String str : write) {
                writer.write(str);
                writer.newLine();
            }
            writer.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println(e);
        }
        return false;
    }

}
